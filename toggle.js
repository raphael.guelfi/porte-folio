

// Lorsque la page est chargée
$(document).ready(function($){
    // Je rajoute la classe none à toutes les div enfant direct de .G2
    $('.G2 > div').addClass('none');
    $("#div1").removeClass('none');
    // Lorsque je clique sur un lien "a" enfant direct de .G1
    $('.G1 > a').click(function(){
        // je récupère le numéro de div dans la target de la balise "a" avec $(this).attr('target'). 
        // $(this) c'est là ou on vient de cliquer
        // .attr('target') c'est pour récupérer l'attribut 'target'
        // Je crée une variable avec #div + le numéro de div
        var matarget = "#div"+$(this).attr('target');
        // Je rajoute la classe none à toutes les div enfant direct de .G2
        // En réalité, ça ajoute la classe none qu'a une seule div, les autres sont déjà cachées en ligne 6
        $('.G2 > div').addClass('none');
        // Ensuite, j'enlève la classe none sur ma Target en utilisant la variable crée en ligne 14
        $(matarget).removeClass('none');
    });

});

