$(document).ready(function(){
  $('.owl-carousel').owlCarousel({
    loop:true,
    items:1,
    margin:10,
    autoHeight:true,
    nav:true,
    center: true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})

})